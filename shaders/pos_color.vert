#version 330 core

layout(location = 0) in vec3 in_position;
layout(location = 1) in vec3 in_color;

out vec3 vs_vertexcolor;

uniform mat4 view_matrix;
uniform mat4 proj_matrix;

void main()
{
	gl_Position = proj_matrix * view_matrix * vec4(in_position, 1);
	vs_vertexcolor = in_color;
}