#version 330

#define	MAX_BONES 120

layout(location=0) in vec3 	in_position;
layout(location=1) in vec3 	in_normal;
layout(location=2) in vec2 	in_texcoord;
layout(location=3) in uvec4 in_bone_indices;
layout(location=4) in vec4	in_bone_weights;

out vec3 vs_vertexnormal;
out vec2 vs_texcoord;

uniform mat4 view_matrix;
uniform mat4 proj_matrix;
uniform mat3 norm_matrix;
uniform mat4 bone_matrices[MAX_BONES];

vec4 skin_position(vec4 position)
{
	// TODO: Compute a resulting vertex position influenced by the bones given by in_bone_indices
	// and in_bone_weights

	vec4 result = in_bone_weights[0] * bone_matrices[in_bone_indices[0]] * position;
	result = result + in_bone_weights[1] * bone_matrices[in_bone_indices[1]] * position;
	result = result + in_bone_weights[2] * bone_matrices[in_bone_indices[2]] * position;
	result = result + in_bone_weights[3] * bone_matrices[in_bone_indices[3]] * position;

	return vec4(result.xyz, 1);
}

void main(void)
{
	// Compute a resulting position
	vec4 skinned_position = skin_position(vec4(in_position, 1));

	// This should also be computed like the position, but is not used for this exercise
	vec3 skinned_normal = norm_matrix * in_normal;

	gl_Position = proj_matrix * view_matrix * skinned_position;
	vs_vertexnormal = skinned_normal;
	vs_texcoord = in_texcoord;
}
