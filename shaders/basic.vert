#version 330 core

layout(location = 0) in vec3 in_position;
layout(location = 1) in vec3 in_normal;
layout(location = 2) in vec2 in_texcoord;

out vec3 vs_vertexnormal;
out vec2 vs_texcoord;

uniform mat4 view_matrix;
uniform mat4 proj_matrix;

void main()
{
	gl_Position = proj_matrix * view_matrix * vec4(in_position, 1);
	vs_vertexnormal = mat3(view_matrix) * in_normal;
	vs_texcoord = in_texcoord;
}