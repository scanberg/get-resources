#version 330 core

layout(location = 0) in vec3 in_position;

out vec3 vs_vertexnormal;
out vec2 vs_texcoord;
out float vs_height_value;

uniform mat4 model_matrix;
uniform mat4 view_matrix;
uniform mat4 proj_matrix;

uniform vec2 world_min;
uniform vec2 world_max;
uniform float world_height_scale = 50.f;
uniform float world_height_min = 0.f;

uniform vec2 patch_min;
uniform vec2 patch_max;
uniform vec2 tile_scale = vec2(128.0);

uniform sampler2D heightmap_tex;

vec2 PatchToWorld(in vec2 local_coord)
{
	return patch_min + local_coord * (patch_max - patch_min);
}

vec2 WorldToLocal(in vec2 world_coord)
{
	return (world_coord - world_min) / (world_max - world_min);
}

void main()
{	
	vec3 world_pos;
	vec2 tc;
	float height;

	world_pos.xz = PatchToWorld(in_position.xz);
	tc = WorldToLocal(world_pos.xz);
	height = textureLod(heightmap_tex, tc, 0).r;
	world_pos.y = world_height_min + height * world_height_scale;

	gl_Position = proj_matrix * view_matrix * vec4(world_pos, 1);
	vs_texcoord = tc * tile_scale;
	vs_height_value = height;
}