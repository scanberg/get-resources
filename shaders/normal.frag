#version 330 core

in vec3 vs_vertexnormal;
layout(location = 0) out vec4 out_color;

void main()
{	
	out_frag = vec4(vs_vertexnormal, 1);
}