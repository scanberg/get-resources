#version 330 core

in vec2 vs_texcoord;
in float vs_height_value;

// Texture layers
uniform sampler2D texture0;
uniform sampler2D texture1;
uniform sampler2D texture2;
uniform sampler2D texture3;

uniform sampler2D noise_tex;

uniform int num_layers = 4;
uniform int debug_mode = 0;

layout(location = 0) out vec4 out_color;

float double_smoothstep(float edge0, float edge1, float edge2, float edge3, float x)
{
	return smoothstep(edge0, edge1, x) * (1 - smoothstep(edge2, edge3, x));
}

void main()
{
	vec3 layer[4];

	layer[0] = texture(texture0, vs_texcoord).rgb;
	layer[1] = texture(texture1, vs_texcoord).rgb;
	layer[2] = texture(texture2, vs_texcoord).rgb;
	layer[3] = texture(texture3, vs_texcoord).rgb;

	float noise = texture(noise_tex, vs_texcoord * 0.25).r;
	float height = vs_height_value + 0.25 * (noise - 0.5);

	vec3 color = vec3(0);
	color += layer[0] * double_smoothstep(-1.0, 0.0, 0.20, 0.25, height);
	color += layer[1] * double_smoothstep(0.20, 0.25, 0.50, 0.55, height);
	color += layer[2] * double_smoothstep(0.50, 0.55, 0.75, 0.80, height);
	color += layer[3] * double_smoothstep(0.75, 0.80, 1.0, 1.5, height);

	if (debug_mode == 1)
		color = vec3(1);

   	out_color = vec4(color, 1);
}