#version 330 core

layout(location = 0) in vec3 in_position;
layout(location = 1) in vec3 in_normal;
layout(location = 2) in vec2 in_texcoord;

out vec3 vs_vertexnormal;
out vec2 vs_texcoord;

uniform mat4 view_matrix;
uniform mat4 proj_matrix;
uniform mat3 norm_matrix;

uniform vec4 plane = vec4(0,0,-1,0);
float gl_ClipDistance[1];

void main()
{
	vec4 view_pos = view_matrix * vec4(in_position, 1);
	gl_ClipDistance[0] = dot(view_pos, plane);
	gl_Position = proj_matrix * view_pos;
	vs_vertexnormal = norm_matrix * in_normal;
	vs_texcoord = in_texcoord;
}