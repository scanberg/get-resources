#version 330

#define	MAX_BONES 120

layout(location=0) in vec3 	in_position;
layout(location=1) in vec3 	in_normal;
layout(location=2) in vec2 	in_texcoord;
layout(location=3) in uvec4 in_bone_indices;
layout(location=4) in vec4	in_bone_weights;

out vec3 vs_vertexnormal;
out vec2 vs_texcoord;

uniform mat4 view_matrix;
uniform mat4 proj_matrix;
uniform mat3 norm_matrix;
uniform mat4 bone_matrices[MAX_BONES];

vec4 skin_position(uvec4 indices, vec4 weights, vec4 position)
{
	vec4 result = weights[0] * bone_matrices[indices[0]] * position;
	result = result + weights[1] * bone_matrices[indices[1]] * position;
	result = result + weights[2] * bone_matrices[indices[2]] * position;
	result = result + weights[3] * bone_matrices[indices[3]] * position;

	return vec4(result.xyz, 1);
}

vec3 skin_normal(uvec4 indices, vec4 weights, vec3 normal)
{
	vec3 result = weights[0] * mat3(bone_matrices[indices[0]]) * normal;
	result = result + weights[1] * mat3(bone_matrices[indices[1]]) * normal;
	result = result + weights[2] * mat3(bone_matrices[indices[2]]) * normal;
	result = result + weights[3] * mat3(bone_matrices[indices[3]]) * normal;

	return normalize(result);
}

void main(void)
{
	vec4 skinned_position = skin_position(in_bone_indices, in_bone_weights, vec4(in_position, 1));
	vec3 skinned_normal = norm_matrix * in_normal;

	gl_Position = proj_matrix * view_matrix * skinned_position;
	vs_vertexnormal = skinned_normal;
	vs_texcoord = in_texcoord;
}
