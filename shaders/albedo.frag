#version 330 core

in vec3 vs_vertexnormal;
in vec2 vs_texcoord;

uniform sampler2D texture0;	// diffuse_map

layout(location = 0) out vec4 out_color;

void main()
{
	vec3 N = normalize(vs_vertexnormal);
	vec3 diffuse = texture(texture0, vs_texcoord).rgb;
   	out_color = vec4(diffuse.xyz, 1);
}