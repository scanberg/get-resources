#version 330 core

layout(location = 0) in vec3 in_position;

uniform mat4 view_matrix;
uniform mat4 proj_matrix;

uniform vec4 plane;
float gl_ClipDistance[1];

void main()
{
	gl_ClipDistance[0] = dot(vec4(in_position, 1), plane);
	gl_Position = proj_matrix * view_matrix * vec4(in_position, 1);
}