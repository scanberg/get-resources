#version 330 core

in vec4 gs_color;
in vec2 gs_texcoord;

uniform sampler2D texture0;

layout(location = 0) out vec4 out_color;

void main()
{
    vec4 tex_color = texture(texture0, gs_texcoord);
    vec4 result = gs_color.rgba * tex_color.rgba;
    out_color = vec4(result);
}