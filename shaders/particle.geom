#version 330 core

layout(points) in;
layout(triangle_strip, max_vertices = 4) out;

uniform mat4 proj_matrix;
uniform float particle_size = 0.1;

in vec4 vs_color[];

out vec4 gs_color;
out vec2 gs_texcoord;

void main()
{
    gs_color = vs_color[0];
    vec4 p = gl_in[0].gl_Position;
    p.w = 1;

    // bottom-left
    vec2 bl = p.xy + vec2(-0.5, -0.5) * particle_size;
    gl_Position = proj_matrix * vec4(bl, p.zw);
    gs_texcoord = vec2(0, 0);
    EmitVertex();

    // bottom-right
    vec2 br = p.xy + vec2(0.5, -0.5) * particle_size;
    gl_Position = proj_matrix * vec4(br, p.zw);
    gs_texcoord = vec2(1, 0);
    EmitVertex();

    // top-left
    vec2 tl = p.xy + vec2(-0.5, 0.5) * particle_size;
    gl_Position = proj_matrix * vec4(tl, p.zw);
    gs_texcoord = vec2(0, 1);
    EmitVertex();

    // top-right
    vec2 tr = p.xy + vec2(0.5, 0.5) * particle_size;
    gl_Position = proj_matrix * vec4(tr, p.zw);
    gs_texcoord = vec2(1, 1);
    EmitVertex();

    EndPrimitive();
}