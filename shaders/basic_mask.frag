#version 330 core

in vec3 vs_vertexnormal;
in vec2 vs_texcoord;

uniform sampler2D texture0; // diffuse_map
uniform sampler2D texture2; // mask_map

layout(location = 0) out vec4 out_color;

void main()
{
	vec3 diffuse = texture(texture0, vs_texcoord).rgb;
	float mask = texture(texture2, vs_texcoord).r;

    if (mask < 0.5)
        discard;
	
   	out_color = vec4(diffuse.xyz, 1);
}