#version 330 core

layout(location = 0) in vec3 in_position;
layout(location = 1) in vec4 in_color;

out vec4 vs_color;

void main()
{
    // in_position is given in view space
	gl_Position = vec4(in_position, 1);
    vs_color = in_color;
}